package io.caltamirano.seguridad.adapter.in.web.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ListarUsuarioResponse {

	String id;
	
	String apellidos;
	
	String nombres;
	
}
