package io.caltamirano.seguridad.adapter.in.web.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CrearEmpresaResponse {

	String id;
	
	String ruc;
	
	String razonSocial;
	
}
